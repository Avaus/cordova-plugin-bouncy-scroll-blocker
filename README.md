Cordova BouncyScrollBlocker plugin
===================================

This is a Cordova plugin that disables the bouncy scroll effect on Windows Phone.

## License

Apache License 2.0